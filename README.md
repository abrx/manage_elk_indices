# manage-eck

Purge, compression des index de la stack de monitoring ELK/ECK via la librairie [elasticsearch](https://elasticsearch-py.readthedocs.io/en/7.9.1/) et python 3.7.

Ce repo permet l'utilisation en direct et construit une image python qui peut être déployée en CronJob dans le cas d'ECK.

## Setup d'un env de test

```bash
# Création d'un virtualenv
python3.7 -m venv ./eck

# Activation du virtualenv (le nom apparaît entre parenthèses dans le prompt)
source ./eck/bin/activate

# Installation des dépendances
pip3 install -r requirements.txt
```

Pour lancer directement le script, il faut définir les variables d'environnement ES_URL et ES_PWD correspondant à l'Elasticseach cible :

```shell
export ES_URL="<url>"
export ES_PWD="<pwd>"
python manage_indices.py
```

Le fichier de configuration utilisé est `config/retention.yml` présent dans le dépôt.
Dans le cas d'une utilisation en job Kubernetes, il faut monter une configmap au même endroit dans le pod et utiliser un secret et une configmap pour déclarer les variables d'environnement.
