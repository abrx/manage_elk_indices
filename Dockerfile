FROM python:3.10.8-slim-bullseye

WORKDIR /usr/src/app

USER root

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY manage_indices.py .

CMD ["python", "./manage_indices.py"]
