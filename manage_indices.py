from elasticsearch import Elasticsearch,exceptions
from datetime import datetime
from yaml import load,FullLoader
from os import environ

"""
    Module pour gérer les index d'un cluster Elasticsearch déployé via ECK sur Kubernetes.
    Le cycle de vie des index est le suivant:
    - hot: écritures et recherches dans l'index
           => plusieurs shards, création continue de segments Lucene
    - warm: recherches mais plus d'écriture (ex: index journalier de la veille).
           => flush et merge en un seul segment Lucene
    - cold: peu de recherches, juste de la rétention de données
           => un seul shard (shrink), index fermé
    - suppression de l'index

    Le traitement peut ensuite être schedulé par cronjob
"""


#####################################################################################
#                                                                                   #
#   Configuration                                                                   #
#                                                                                   #
#####################################################################################

# Variables définies dans l'environnement, à la main pour dev ou dans le manifeste du CronJob 
es_url = environ.get('ES_URL')
es_user = 'elastic'
es_pwd = environ.get('ES_PWD')

if not es_url:
    raise ValueError("La variable es_url n'est pas définie")
elif not es_pwd:
    raise ValueError("La variable es_pwd n'est pas définie")

# Connexion à Elasticsearch
try:
    es = Elasticsearch([es_url], http_auth=(es_user,es_pwd), verify_certs=False, ca_certs=False)
except:
    print("Erreur lors de la connexion à Elasticsearch à", es_url)
    raise
else:
    print("Connexion à Elasticsearch établie")


#####################################################################################
#                                                                                   #
#   Fonctions                                                                       #
#                                                                                   #
#####################################################################################


def get_config(config_file="config/retention.yml"):
    """
        Récupère Les 3 dictionnaires :
          - list_indices_time2warm
          - list_indices_time2cold
          - list_indices_retention
        contenant chacun un pattern d'index associé à une durée en jours

        Exemple: 
        list_indices_retention:
          "traefik-*": 5
    """
    print("Récupération de la configuration dans", config_file)
    
    with open(config_file) as conf:

        try:
            return load(conf, Loader=FullLoader)

        except:
            print("erreur lors de la récupération de la configuration", config_file)
            raise


def get_indices(index_pattern="*"):
    """
        Récupère une liste d'index + date de création selon un pattern d'index fourni
    """
    try:
        return es.cat.indices(index=index_pattern, h=["index"]) 

    except:
        print("erreur sur la connexion à Elasticsearch à ",es_url)
        raise


def has_passed(index,retention):
    """
        True si la date de création est antérieure à la date de création de l'index
        La rétention est en jours
    """
    try:
        # retourne un timestamp epoch en millisecondes qu'on convertit en datetime
        index_creation_time = datetime.fromtimestamp(int(int(es.cat.indices(index=index, h=["creation.date"])) / 1000))

    except exceptions.NotFoundError:
        # Gère les index non existant, souvent des strings vides
        return False
        
    except ValueError:
        # Gère les index non existant, souvent des strings vides
        return False

    except :
        print("erreur sur l'index: ",index)
        raise

    if (datetime.today() - index_creation_time).days >= int(retention):
        return True
    
    return False


def merge_index(index):
    """
        Opère un flush & force merge des segments Lucene sur les index passés en warn
        Il se peut que l'opération tombe en timeout, auquel cas elle continuera côté serveur Elasticsearch
    """
    try:
        es.indices.forcemerge(index=index, flush="true", max_num_segments=1, request_timeout=120)
    
    except (exceptions.ConnectionTimeout):
        print("Timeout en mergeant l'index:",index)

    except:
        print("Erreur en mergeant l'index:",index)
        raise

    else:
        print("index mergé:",index)
    

def close_index(index):
    """
        Ferme les index passés en paramètre. 
        Attention à ne pas fermer des index en cours d'écriture.
    """
    try:
        es.indices.close(index=index, ignore_unavailable="true", request_timeout=120)
    
    except (exceptions.ConnectionTimeout):
        print("Timeout en fermant l'index:",index)

    except:
        print("Erreur en fermant l'index:",index)
        raise

    else:
        print("index fermé:",index)


def delete_index(index):
    """
        Supprime les index ayant passé la période de rétention
    """  
    try:
        es.indices.delete(index)

    except:
        print("erreur en supprimant l'index",index)
        raise

    else:
        print("index supprimé:",index)


#####################################################################################
#                                                                                   #
#   Main                                                                            #
#                                                                                   #
#####################################################################################


def main():
    """"
        Récupère le fichier de configuration
        Supprime les index périmés puis merge les index tièdes
    """
    # Récupération de la configuration
    data= get_config()
    list_indices_retention, list_indices_time2cold, list_indices_time2warm = data["list_indices_retention"], data["list_indices_time2cold"], data["list_indices_time2warm"]

    print("Supression des index périmés...")
    for index_pattern, retention in list_indices_retention.items():

        for index in get_indices(index_pattern).split("\n"):

            if has_passed(index, retention):
            
                delete_index(index)

    print("Merge des index warm qui passent en readonly...")
    for index_pattern, time2warn in list_indices_time2warm.items():

        for index in get_indices(index_pattern).split("\n"):

            if has_passed(index, time2warn):

                merge_index(index)

    print("Fermeture des index qui passent froids...")
    for index_pattern, time2warn in list_indices_time2cold.items():

        for index in get_indices(index_pattern).split("\n"):

            if has_passed(index, time2warn):

                close_index(index)

    print("Done !")


def test():
    # print(has_passed("access-traefik-2020.10.21","5"))
    
    # print(get_indices("traefik-*"))
    # print(int(int(es.cat.indices(index="access-traefik-2020.10.26", h=["creation.date"])) / 1000))

    # get_config()


    # data= get_config()
    # print(data)

    # list_indices_retention, list_indices_time2cold, list_indices_time2warm = data["list_indices_retention"], data["list_indices_time2cold"], data["list_indices_time2warm"] 

    # print("retention:", list_indices_retention)
    # print("cold::", list_indices_time2cold)
    # print("warm::", list_indices_time2warm)
    pass


if __name__ == "__main__":
    main()
